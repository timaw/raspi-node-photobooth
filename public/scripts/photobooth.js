var stream;
var pb = {};

// Start up
pb.init = function() {
	var socket = io();
	socket.on('take photo', function(msg) {
		pb.takePhoto();
	});

	pb.startVideo();
}
// Start video
pb.startVideo = function() {
	
	// Set firefox/iceweasel getUserMedia	
	navigator.getUserMedia = navigator.mozGetUserMedia;

	// Set getUserMedia constraints
	var constraints = {
		video: { width: 1280, height: 720 }
	};

	// On success
	function onSuccess(stream) {
		
		var video = document.querySelector('video');
		
		video.src = window.URL.createObjectURL(stream);
		video.onloadedmetadata = function(e) {
			video.play();
		};
		
		pb.stream = stream;
	}
	// On error
	function onError(err) {
		console.log('The following getUserMedia error occurred: ' + err.name);
	}	
	//
	navigator.getUserMedia(constraints, onSuccess, onError);
}

// 
pb.takePhoto = function() {


	// Open model
	
	var currentSecond = 3;

	// Display time
	document.getElementById('countdown').innerHTML = currentSecond;
	// Unhide modal
	document.getElementById('model').style.display = '';

	// Clear picture
	document.getElementById('finish').innerHTML = ' ';

	pb.stream.stop();

	// Countdown timer
 	var interval = setInterval(function() {
			
			currentSecond--;

			// Display time
			document.getElementById('countdown').innerHTML = currentSecond;
			// Unhide modal
			document.getElementById('model').style.display = '';
			if (currentSecond == 3) {
					// Stop video feed
					console.dir(pb.stream);
					pb.stream.stop();
			}
        	if (currentSecond == 1) {
            		clearInterval(interval);

				$.ajax({
					type: "POST",
					url: '/takePhoto',
					success: function(response){

						console.dir(response);

						

//						document.getElementById('finish').src = response;
						document.getElementById('finish').innerHTML = '<img src="'+response.url+'" style="height:720px;width:1280px;" />';

						//  Start video feed
						pb.startVideo();

	            		document.getElementById('start').style.display = 'none';
						document.getElementById('finish').style.display = '';

						setTimeout(function() {
							// Hide modal
							document.getElementById('start').style.display = '';
//							document.getElementById('finish').style.display = 'none';
							document.getElementById('finish').innerHTML = '';
							document.getElementById('model').style.display = 'none';
						 }, 5000);
					}
				});

        	}

        	
	
    	}, 1000);

}
	// Trigger ajax call to run photo command, return picture url
	//setTimeout(function() {
		
//	}, 3000);
