module.exports = function(io) {

var express = require('express');
var router = express.Router();
var exec = require('child_process').exec;
var app = require('express');
//var http = require('http').Server(app);
//var io = require('socket.io')(http);
//app.io = require('socket.io')();
//var gpio = require('pi-gpio');

//	gpio.open(7, 'input', function(err) {
//	});

io.on('connection', function(socket) {
	console.log('a user connected');
});

var buttonActivated = false;
var Gpio = require('onoff').Gpio;
var button = new Gpio(4, 'in', 'both');

button.watch(function(err, value) {
	if(err) {
		console.log('ERROR' + error);
	}
	if(value === 0) {
	} else {
//		console.log('button released');

	}
		if(!buttonActivated) {
			buttonActivated = true;
			io.emit('take photo', {});
			console.log('button pressed and emitted');
		} else {
			console.log('button pressed, but not emitted');
		}
});

/* GET home page. */
router.get('/', function(req, res, next) {

		
//	while(true) {
//		gpio.read(7, function(err, value) {
//			if(err) throw err;
//			if( value === 0) {
//				console.log('Button Released');
//			} else {
//				console.log('Button Pressed!');
//			}
//		});
//	}

	res.render('index', { title: 'Express' });
});

router.post('/takePhoto', function(req, res){

	var cmd = 'raspistill ';
	var d = new Date();
	var day = d.getDate();
	var month = d.getMonth() + 1;
	var year = d.getFullYear();
	var datestamp = month + '-' + day + '-' + year;
	var hour =  d.getHours();
	var minute = d.getMinutes();
	var seconds = d.getSeconds();

	if(hour<10) {
		hour = '0' + hour;
	}
	if(seconds<10) {
		seconds = '0' + seconds;
	}
	var timestamp = hour + ':' + minute + ':' + seconds;

	var filename = datestamp + ' ' + timestamp + '.jpg';
	var url = process.env.PWD + '/public/' + filename;
	
	// Build command
	cmd += '-n -t 3 -drc low -ex auto -co 30 -ISO 800 -o "'+ url + '"';
	// -vf -t 5  -ss 1000
	// Start

	console.log('Taking photo - ' + Date());

    var child = exec(cmd, function(error, stdout, stderr){
		console.log('stdout: ' + stdout);
    	console.log('stderr: ' + stderr);		
		if (error !== null) {
      		console.log('exec error: ' + error);
    	}
		buttonActivated = false;
		console.log('Done taking photo - ' + Date());
		res.json({ url: filename });
	}); 
	

});

//module.exports = router;
 return router;
}
