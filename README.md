# raspi-node-photobooth (in development)
A Raspberry Pi photo booth web application written in node.js

## Initial Setup

### Pi Initial Setup

####Change firefox setting to always allow webcam access
Set about:config media.navigator.permission.disabled = true

####Add camera at boot to /etc/modules
sudo nano /etc/modules
add "bcm2835-v4l2" to bottom of list on new line
to exit and save nano, do
	-ctrl + x 
	-y
	- enter

FYI, to do load camera driver manually, run this command
sudo modprobe bcm2835-v4l2


